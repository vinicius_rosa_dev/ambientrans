import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import PaginaPrincipal from './screens/Home';
import MeioAmbiente from './screens/MeioAmbiente';
import Transporte from "./screens/Transporte";

const MainNavigator = createStackNavigator({
  Home: {screen: PaginaPrincipal},
  MeioAmbiente: {screen: MeioAmbiente},
  Transporte: {screen: Transporte},
});

const App = createAppContainer(MainNavigator);

export default App;