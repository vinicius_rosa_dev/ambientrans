import * as React from 'react';
import {
  Linking,
  SafeAreaView,
  ScrollView
 } from 'react-native';
import api from '../services/api';
import { ListItem } from 'react-native-elements'

class Transporte extends React.Component {
  static navigationOptions = {
    title: "Araraquara - Transporte"
  }

  state = {
    noticias: []
  }

  noticiasTransporte = async () => {
    const response = await api.get('/noticiastransporte.php');
    var noticiasTransporte = response.data;

    this.setState({
      noticias: noticiasTransporte
    });
  }

   componentDidMount(){
    this.noticiasTransporte();
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
       <SafeAreaView>
       <ScrollView>{
        this.state.noticias.map((noticia, index) => (
          <ListItem
            key={index}
            title={noticia.tituloNoticia}
            titleStyle={{ color: 'black', fontWeight: 'bold' }}
            subtitle={noticia.descricaoNoticia}
            subtitleStyle={{ fontStyle: "italic" }}
            leftAvatar={{source: { uri: noticia.imagemNoticia}, size: "large", rounded: false}}
            onPress={() => {
              Linking.openURL(noticia.urlNoticia);
            }}
            bottomDivider
          />
        ))
        }
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default Transporte;