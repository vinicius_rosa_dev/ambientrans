import * as React from 'react';
import { 
  View,
  Image,
  StyleSheet,
  ImageBackground,
 } from 'react-native';
 import { Button } from 'react-native-elements'

class Botao extends React.Component {
  render () {
    return (
      <View style={styles.botao}>
      <Button onPress={this.props.onPress}
      title={this.props.title}
      buttonStyle={{backgroundColor: this.props.color}}
      titleStyle={{
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 20
        }}/>
      </View>
    );
  }
}

export default class PaginaPrincipal extends React.Component {

  static navigationOptions = {
    title: "AmbienTrans - Notícias de Araraquara"
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <ImageBackground source={require('../images/imagem_fundo.jpg')} style={styles.container}>
        <View style={styles.overlay}>
        <Image style={styles.imagemLogo} source={require('../images/ambientrans.png')} />
        <Botao onPress={() => navigate('MeioAmbiente')} title="Meio-Ambiente" color="green" />
        <Botao onPress={() => navigate('Transporte')} title="Transporte" color="blue" />
        </View>
      </ImageBackground>
    );
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  botao: {
    marginTop: '10%',
    marginBottom: '5%',
    width: '90%',
    borderRadius:10,
    borderWidth: 1,
  },
  tituloApp:{
    fontSize: 25,
    fontWeight: "bold",
  },
  overlay:{
    width: '88%',
    height: '88%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  imagemLogo:{
    width: '80%',
    height: '50%'
  }
});